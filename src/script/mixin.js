import { URL_BASE } from './Constant'
export default {
    methods: {
        async sendData (dataObject){
           var result = await this.$http.post(URL_BASE + 'execute', dataObject, { headers: { "content-type": "application/json" } })
           return result
        },
        async sendFile (dataObject){
            var result = await this.$http.post(URL_BASE + 'upload-file-single', dataObject, { headers: {  "Content-Type": "multipart/form-data" } })
            return result
        },
        async getData (URL){
            var result = await this.$http.get(URL, { headers: { "content-type": "application/json" } })
            return result
         },
        async getFile (dataObject){
            var result = await this.$http.post(URL_BASE + 'file-base64', dataObject, { headers: { "content-type": "application/json" } })
            return result
         },

    },
} 