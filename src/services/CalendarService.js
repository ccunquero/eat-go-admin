export default {
    insUpdDetailMenu(context, object, isLastDetail){
        var requestBody = {
           "sp" : "sp_insUpdDetailMenu",
           "model" : object
       }
       this.$q.loading.show()
       this.$root.sendData(requestBody).then((result)=>{
           
           let code = result.data.code
           console.log(result)
       
           if(code == 200){
               if(isLastDetail){       
                   if(this.isUpdateMenu) {
                       let message = result.data.data[0].Message     
                       notifyMessage.showNotify(context.$q,code, message)
                   }
                   else
                       notifyMessage.showNotify(context.$q,code, "Menu asignado correctamente")
                   this.$data.data = this.getData(false)
                   this.$forceUpdate();
                   this.resetValues()
                   this.$q.loading.hide()   
           
               }
           }else{
           
               notifyMessage.showNotify(context.$q,500, "Error al agregar menu")
                  this.$q.loading.hide() 
                 
           }

       }).catch((error)=> {
           notifyMessage.showNotifyErrorSrv(this.$q)
           console.log(error)
           this.$q.loading.hide()  
           this.resetValues()
           
       });

   },
}