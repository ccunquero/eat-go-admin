
const routes = [
  {
    path: '/',
    meta: {
      public: true,
    },
    component: () => import('components/Login/Login.vue'),
  },
  {
    path: '/Home',
    meta: {
      public: false,
    },
    component: () => import('components/Home/Home.vue'),
    children: [
    
    ]
  },
  
 
  {
    path: '/User',
    meta: {
      public: false,
    },
    component: () => import('components/User/UserList.vue'),
    children: [
    
    ]
  },
  
  {
    path: '/Login',
    meta : {
      public : true,
    },
    component: () => import('components/Login/Login.vue'),
    children: [
    
    ]
  },
  {
    path: '/Menu',
    meta : {
      public : false,
    },
    component: () => import('components/Menu/MenuList.vue'),
    children: [
    
    ]
  },
  {
    path: '/MenuOffer',
    meta : {
      public : false,
    },
    component: () => import('components/MenuOffer/MenuOfferList.vue'),
    children: [
    
    ]
  },
  {
    path: '/Offer',
    meta: {
      public: false,
    },
    component: () => import('components/Offer/OfferList.vue'),
    children: [
    
    ]
  },
  {
    path: '/ReportMenu',
    meta: {
      public: false,
    },
    component: () => import('components/Reports/ReportMenu.vue'),
    children: [
    
    ]
  },
  {
    path: '/ReportRecharge',
    meta: {
      public: false,
    },
    component: () => import('components/Reports/ReportRecharge.vue'),
    children: [
    
    ]
  },
  {
    path: '/ReportConsumption',
    meta: {
      public: false,
    },
    component: () => import('components/Reports/ReportConsumption.vue'),
    children: [
    
    ]
  },
  {
    path: '/RequestTray',
    meta: {
      public: false,
    },
    component: () => import('components/RequestTray/RequestTrayList.vue'),
    children: [
    
    ]
  },
  {
    path: '/RequestQR',
    meta: {
      public: false,
    },
    component: () => import('components/RequestQrPrint/RequesQrList.vue'),
    children: [
    
    ]
  },
  {
    path: '/Calendar',
    meta: {
      public: false,
    },
    component: () => import('components/Calendar/Calendar.vue'),
    children: [
    
    ]
  },
  

]
// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    meta: {
      public: false
    },
    component: () => import('pages/Error404.vue')
  })
}

export default routes
