const Menu =[
    {
        title : ' Inicio',
        icon : 'fa fa-home',
        component : 'Home'
    },
   
    {
        title : ' Usuarios',
        icon : 'fas fa-user',
        component : 'User'
    },
    {
        title : ' Menus',
        icon : 'fas fa-utensils',
        component : 'Menu'
    },
    {
        title : 'Calendarizar Menus',
        icon : 'calendar_today',
        component : 'Calendar'
    },
    // {
    //     title : 'Ofertas',
    //     icon : 'fas fa-mobile',
    //     component : 'Offer'
    // },
   
    
    {
        title : 'Bandeja Solicitudes Saldo',
        icon : 'fas fa-clipboard',
        component : 'RequestTray'
    },
    {
        title : 'Bandeja Impresiones QR',
        icon : 'fas fa-qrcode',
        component : 'RequestQR'
    },
    {
        title : ' Anuncios',
        icon : 'fas fa-tags',
        component : 'MenuOffer'
    },
    {
        title : 'Reporte Menus Diarios',
        icon : 'fas fa-list-alt',
        component : 'ReportMenu'
    },
    {
        title : 'Reporte Recargas',
        icon : 'fas fa-list-alt',
        component : 'ReportRecharge'
    },
    {
        title : 'Reporte Consumo',
        icon : 'fas fa-list-alt',
        component : 'ReportConsumption'
    },
    {
        title : ' Salir',
        icon : 'fas fa-sign-out-alt',
        component : 'Login'
    },
    
   
]

export default Menu;